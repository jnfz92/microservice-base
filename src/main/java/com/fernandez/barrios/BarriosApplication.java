package com.fernandez.barrios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BarriosApplication {

	public static void main(String[] args) {
		SpringApplication.run(BarriosApplication.class, args);
	}

}
